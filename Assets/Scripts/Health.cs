﻿using System;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] protected float _value;
    public HealthData HealthData;

    public event Action ValueLow;
    public event Action<Health> ValueChanged;

    private void Awake()
    {
        Value = HealthData.StartValue;
    }

    public float Value
    {
        get { return _value; }
        set
        {
            if (_value == value)
                return;
            _value = Mathf.Clamp(value, HealthData.MinValue, HealthData.MaxValue);
            ValueChanged?.Invoke(this);
            if (_value <= HealthData.MinValue)
                ValueLow?.Invoke();

        }
    }

    public float RelativeValue => _value / (HealthData.MaxValue - HealthData.MinValue);

    public bool IsAlive()
    {
        return _value > HealthData.MinValue;
    }
}
