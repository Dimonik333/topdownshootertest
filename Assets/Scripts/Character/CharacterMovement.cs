﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class CharacterMovement : MonoBehaviour
{
    private Rigidbody2D _rigidbody;
    private Transform _transform;

    private Vector3 _lookTarget;
    private Vector2 _moveVector;

    public Vector2 MoveSpeed;

    private void Awake()
    {
        _transform = GetComponent<Transform>();
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    // Развернуть персонажа к цели
    public void SetLook(Vector3 target)
    {
        _lookTarget = target;
    }
    
    // Переместить персонажа на заданный вектор
    public void SetMove(Vector2 moveVector)
    {
        _moveVector = moveVector;
    }

    private void FixedUpdate()
    {          
        Vector2 movement = Vector2.Scale(_moveVector, MoveSpeed) * Time.deltaTime;
        _rigidbody.MovePosition(_rigidbody.position += movement);

        var lookDirection = (Vector2)_lookTarget - _rigidbody.position;
        var rotation = Quaternion.LookRotation(Vector3.forward, lookDirection);
        _rigidbody.SetRotation(rotation);
    }
}
