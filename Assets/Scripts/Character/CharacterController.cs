﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    public Camera MainCamera;
    public CharacterMovement Movement;
    public CharacterShooter Shooter;

    void Update()
    {
        var horizontal = Input.GetAxis("Horizontal");
        var vertical = Input.GetAxis("Vertical");
        var moveVector =  new Vector2(horizontal, vertical);

        var mousePosition = MainCamera.ScreenToWorldPoint(Input.mousePosition);

        Movement.SetMove(moveVector);
        Movement.SetLook(mousePosition);

        Shooter.Look(mousePosition);

        if (Input.GetMouseButtonDown(0))        
            Shooter.Fire(mousePosition, WeaponHand.Left);        
        else if (Input.GetMouseButtonUp(0))
            Shooter.Release( WeaponHand.Left);

        if (Input.GetMouseButtonDown(1))
            Shooter.Fire(mousePosition, WeaponHand.Right);
        else if (Input.GetMouseButtonUp(1))
            Shooter.Release(WeaponHand.Right);
    }
}
