﻿using UnityEngine;

public class CharacterShooter : MonoBehaviour
{
    [Header("Weapons")]
    public Weapon LeftWeapon;
    public Weapon RightWeapon;
    [Header("Slots")]
    public Transform LeftWeaponSlot;
    public Transform RightWeaponSlot;

    // наведение на цель
    public void Look(Vector2 target)
    {
        Vector2 leftDirection = target - (Vector2)LeftWeaponSlot.position;
        LeftWeaponSlot.rotation = Quaternion.LookRotation(Vector3.forward, leftDirection);

        Vector2 rightDirection = target - (Vector2)RightWeaponSlot.position;
        RightWeaponSlot.rotation = Quaternion.LookRotation(Vector3.forward, rightDirection);
    }

    // открыть огонь / зажать курок
    public void Fire(Vector2 target, WeaponHand hand)
    {
        switch (hand)
        {
            case WeaponHand.Left: LeftWeapon.Fire(target); break;
            case WeaponHand.Right: RightWeapon.Fire(target);break;
        }
    }

    // остановить огонь / отпустить курок
    public void Release(WeaponHand hand)
    {
        switch (hand)
        {
            case WeaponHand.Left: LeftWeapon.Release(); break;
            case WeaponHand.Right: RightWeapon.Release(); break;
        }
    }
}

public enum WeaponHand
{
    Left,
    Right
}
