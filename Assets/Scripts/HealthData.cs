﻿using UnityEngine;

[CreateAssetMenu(fileName = "HealthData", menuName = "GameData/HealthData")]
public class HealthData : ScriptableObject
{
    public float StartValue;
    public float MinValue;
    public float MaxValue;
}
