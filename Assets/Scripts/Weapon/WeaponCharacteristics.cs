﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Характеристики оружия
/// </summary>
[CreateAssetMenu(fileName = "Weapon Characteristics", menuName = "Weapon/Characteristics")]
public class WeaponCharacteristics : ScriptableObject
{
    public float ShotInterval = 0.1f;   // интервал между выстрелами
    public int SpreadCount = 1;         // снарядов в одном выстреле
    public int BurstCount = 1;          // размер очереди
    public float BurstInterval = 0.1f;  // интервал между выстрелами в очереди
    [Space]
    public float BulletPower = 1f;      // сила снаряда    
    [Space]
    public ShootType ShootType = ShootType.Single;
}

public enum ShootType
{
    Single,
    Auto
}