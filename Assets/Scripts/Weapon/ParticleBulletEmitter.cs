﻿using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Генератор снарядов на основе системы частиц
/// </summary>
[RequireComponent(typeof(ParticleSystem))]
public class ParticleBulletEmitter : BaseBulletEmitter
{
    private ParticleSystem _particleSystem;
    private List<ParticleCollisionEvent> _collisionEvents = new List<ParticleCollisionEvent>();

    public override event Action<HitEventArgs> HitEvent;

    private void Awake()
    {
        _particleSystem = GetComponent<ParticleSystem>();
    }

    public override void Emit(int count)
    {
        _particleSystem.Emit(count);
    }

    public void OnParticleCollision(GameObject other)
    {

        var count = _particleSystem.GetCollisionEvents(other, _collisionEvents);

        if (HitEvent != null)
        {
            foreach (var ce in _collisionEvents)
                HitEvent(new HitEventArgs(other, ce));
        }
    }
}
