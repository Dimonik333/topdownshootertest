﻿using UnityEngine;

public class HitEventArgs
{
    public GameObject Target;       // с кем столкнулись
    public Vector3 Intersection;    // точка столкновения
    public Vector3 Velocity;        // скорость столкновения
    public Vector3 Normal;          // нормаль     

    public HitEventArgs(GameObject target, ParticleCollisionEvent particleCollisionEvent)
    {
        Target = target;
        Intersection = particleCollisionEvent.intersection;
        Velocity = particleCollisionEvent.velocity;
        Normal = particleCollisionEvent.normal;
    }
}
