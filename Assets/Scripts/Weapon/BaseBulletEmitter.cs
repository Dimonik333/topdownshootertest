﻿using System;
using UnityEngine;

/// <summary>
/// Базовый класс генератора снарядов
/// </summary>
public abstract class BaseBulletEmitter : MonoBehaviour
{
    public abstract void Emit(int count);

    public abstract event Action<HitEventArgs> HitEvent;
}
