﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Автоматическая стрельба из оружия
/// </summary>
public class WeaponAutoFireStrategy : WeaponFireStrategy
{
    private Coroutine _fireRoutine;

    public WeaponAutoFireStrategy(Weapon weapon) : base(weapon)
    { }

    public override void Fire()
    {
        if (_fireRoutine != null)
            _weapon.StopCoroutine(_fireRoutine);
        _fireRoutine = _weapon.StartCoroutine(FireRoutine());
    }

    public override void Release()
    {
        if (_fireRoutine != null)       
            _weapon.StopCoroutine(_fireRoutine);           
        
    }

    private IEnumerator FireRoutine()
    {
        while (true)
        {
            if (_shootData.LastShootTime + _characteristics.ShotInterval <= Time.realtimeSinceStartup)
            {
                _weapon.Shot();
                _shootData.LastShootTime = Time.realtimeSinceStartup;
            }
            yield return null;
        }        
    }
}
