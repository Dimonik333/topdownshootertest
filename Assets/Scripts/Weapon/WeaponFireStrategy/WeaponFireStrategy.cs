﻿
/// <summary>
/// Базовый класс для различных режимов стрельбы.
/// </summary>
public abstract class WeaponFireStrategy
{
    protected Weapon _weapon;
    protected WeaponShotData _shootData;
    protected WeaponCharacteristics _characteristics;

    public WeaponFireStrategy(Weapon weapon)
    {
        _weapon = weapon;
        _characteristics = _weapon.Characteristics;
        _shootData = _weapon.ShotData;
    }

    public abstract void Fire();
    public abstract void Release();
}
