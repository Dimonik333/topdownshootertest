﻿using UnityEngine;

/// <summary>
/// Стрельба одиночными из оружия
/// </summary>
public class WeaponSingleFireStrategy : WeaponFireStrategy
{
    public WeaponSingleFireStrategy(Weapon weapon) : base(weapon)
    {
    }

    public override void Fire()
    {
        if (_shootData.LastShootTime + _characteristics.ShotInterval <= Time.realtimeSinceStartup)
        {
            _weapon.Shot();
            _shootData.LastShootTime = Time.realtimeSinceStartup;
        }
    }

    public override void Release()
    {
    }
}
