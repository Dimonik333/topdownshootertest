﻿using System;
using System.Collections;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField] BaseBulletEmitter _emitter;
    [SerializeField] protected WeaponCharacteristics _characteristics;

    public WeaponShotData ShotData { get; protected set; } = new WeaponShotData();
    public WeaponCharacteristics Characteristics => _characteristics;

    protected Coroutine _shootRoutine;

    public event Action<Enemy> HitEvent;

    protected WeaponFireStrategy _fireStrategy;

    private void Awake()
    {
        SetFireStrategy();
        _emitter.HitEvent += HitEventHandler;
    }

    private void SetFireStrategy()
    {
        switch (_characteristics.ShootType)
        {
            case ShootType.Single: _fireStrategy = new WeaponSingleFireStrategy(this); break;
            case ShootType.Auto: _fireStrategy = new WeaponAutoFireStrategy(this); break;
        }
    }

    private void HitEventHandler(HitEventArgs args)
    {
        var enemy = args.Target.GetComponent<Enemy>();
        if (enemy != null)
            enemy.Health.Value -= _characteristics.BulletPower;
    }

    public void Fire(Vector2 target)
    {
        _fireStrategy.Fire();
    }

    public void Release()
    {
        _fireStrategy.Release();
    }

    public void Shot()
    {
        _shootRoutine = StartCoroutine(ShootRoutine());
    }

    private IEnumerator ShootRoutine(Action callback = null)
    {
        _emitter.Emit(_characteristics.SpreadCount);
        for (int i = 1; i < _characteristics.BurstCount; i++)
        {
            yield return new WaitForSeconds(_characteristics.BurstInterval);
            _emitter.Emit(_characteristics.SpreadCount);
        }
        callback?.Invoke();
    }
}

/// <summary>
/// Данные об оружие в режиме стрельбы
/// </summary>
public class WeaponShotData
{
    public float LastShootTime;
}