﻿using System;
using System.Collections.Generic;
using UnityEngine;

using Random = UnityEngine.Random;

/// <summary>
/// Создание противников/целей и посчет очков по уничтоженным.
/// </summary>
public class EnemiesManager : MonoBehaviour
{
    private float _score;
    private float _elapsedTime;
    private float _spawnDelay;

    private List<Enemy> _enemies = new List<Enemy>();

    public event Action<float> ScoreChanged;

    public Vector2 SpawnAreaSize;
    public Vector2 SpawnAreaCenter;
    [Space]
    public Enemy EnemyPrefab;
    public int EnemyMaxCount;
    [Space]
    public float SpawnDelay;
    [Range(0f, 1f)]
    public float SpawnDelayNoise;
    public Transform EnemiesParent;

    public float Score
    {
         get { return _score; }
        protected set { _score = value; ScoreChanged?.Invoke(_score); }
    }

    private void Update()
    {
        if (_spawnDelay < _elapsedTime)
        {
            _elapsedTime = 0f;

            var noiseSign = Random.Range(-1f, 1f) > 0 ? 1 : -1;
            _spawnDelay = SpawnDelay * (1 + noiseSign * Random.Range(0f, SpawnDelayNoise));

            if (_enemies.Count < EnemyMaxCount)
                SpawnEnemy();
        }

        _elapsedTime += Time.deltaTime;
    }

    private void SpawnEnemy()
    {
        var position = SpawnAreaCenter + new Vector2(
            Random.Range(-SpawnAreaSize.x, SpawnAreaSize.x),
            Random.Range(-SpawnAreaSize.y, SpawnAreaSize.y)) / 2;

        var enemy = Instantiate(EnemyPrefab, position, Quaternion.identity, EnemiesParent);
        enemy.Death += OnEnemyDeath;
        _enemies.Add(enemy);
    }

    private void OnEnemyDeath(Enemy enemy)
    {
        enemy.Death -= OnEnemyDeath;
        Score += enemy.EnemyScore;

        _enemies.Remove(enemy);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireCube(SpawnAreaCenter, SpawnAreaSize);
    }
}
