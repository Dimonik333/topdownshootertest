﻿using System;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Health Health;
    public float EnemyScore = 10f;
    public event Action<Enemy> Death;

    private void Awake()
    {
        Health.ValueLow += OnHealthValueLow;
    }

    private void OnDestroy()
    {
        Health.ValueLow -= OnHealthValueLow;
    }

    private void OnHealthValueLow()
    {
        Death?.Invoke(this);
        Destroy(gameObject);
    }
}
