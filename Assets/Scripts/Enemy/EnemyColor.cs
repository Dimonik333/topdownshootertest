﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Изменение цвета цели в зависимоси от уровня здоровья
/// </summary>
public class EnemyColor : MonoBehaviour
{
    public Health Health;
    public Gradient ColorByHealth;
    public SpriteRenderer[] BodySprites;

    private void Start()
    {
        OnHealthValueChanged(Health);
    }

    private void OnEnable()
    {      
        Health.ValueChanged += OnHealthValueChanged;
    }

    private void OnDisable()
    {
        Health.ValueChanged -= OnHealthValueChanged;
    }

    private void OnHealthValueChanged(Health health)
    {
        for (int i = 0; i < BodySprites.Length; i++)
            BodySprites[i].color = ColorByHealth.Evaluate(1 - health.RelativeValue);
    }
}
