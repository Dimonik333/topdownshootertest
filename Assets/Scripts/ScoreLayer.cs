﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreLayer : MonoBehaviour
{
    public EnemiesManager EnemiesManager;
    public Text ScoreValue;


    private void OnEnable()
    {
        ShowScore(EnemiesManager.Score);
        EnemiesManager.ScoreChanged += ShowScore;
    }

    private void OnDisable()
    {
        EnemiesManager.ScoreChanged += ShowScore;
    }

    private void ShowScore(float score)
    {
        ScoreValue.text = score.ToString();
    }
}
